"use strict"

let buttons = document.querySelectorAll('.btn-wrapper .btn');

// Записуємо змінну activeButton, яка на початку буде пустою, щоб потім туди записати
// кнопку, що була натиснута й в подальшому її зафарбувати в дефолтний колір
let activeButton = null;

// Записуємо які код кнопок, що ми маємо на екрані 
const KEYS = ['Enter','KeyS','KeyE','KeyO','KeyN','KeyL','KeyZ'];

document.addEventListener('keydown', function(event) {
    console.log(event.code);
    buttons.forEach(function(button){

        // Якщо activeButton === true та кнопка, що натиснута, присутня в тих кнопках, що ми
        // маємо на екрані, зафарбовуємо її в дефолтний колір

        // Робимо це для того, щоб при натискані, наприклад кнопки 'X', якої немає на екрані
        // вже зафарбована в синій кнопка не змінювала свій колір
        if ( activeButton && KEYS.includes(event.code) ) {
            button.style.backgroundColor = 'black';
        }
        if (event.code === button.dataset.keyCode) {
            button.style.backgroundColor = 'blue';

            // Записуємо, що кнопка є натиснутою
            activeButton = button;
        }
    })
    
})


// ---------- СТАРИЙ КОД ---------- \\ 

// document.addEventListener('keydown', function(event){
//     // console.log(event);

//     for (let button of buttons) {
//         if (event.code === 'Enter' && button.textContent === "Enter"){
//             for (let btn of buttons) {
//                 btn.style.backgroundColor = 'black';
//             }
//             button.style.backgroundColor = 'blue'; 
//         }
//         if (event.code === 'KeyS' && button.textContent === "S"){
//             for (let btn of buttons) {
//                 btn.style.backgroundColor = 'black';
//             }
//             button.style.backgroundColor = 'blue'; 
//         }
//         if (event.code === 'KeyE' && button.textContent === "E"){
//             for (let btn of buttons) {
//                 btn.style.backgroundColor = 'black';
//             }
//             button.style.backgroundColor = 'blue'; 
//         }
//         if (event.code === 'KeyO' && button.textContent === "O"){
//             for (let btn of buttons) {
//                 btn.style.backgroundColor = 'black';
//             }
//             button.style.backgroundColor = 'blue'; 
//         }
//         if (event.code === 'KeyN' && button.textContent === "N"){
//             for (let btn of buttons) {
//                 btn.style.backgroundColor = 'black';
//             }
//             button.style.backgroundColor = 'blue'; 
//         }
//         if (event.code === 'KeyL' && button.textContent === "L"){
//             for (let btn of buttons) {
//                 btn.style.backgroundColor = 'black';
//             }
//             button.style.backgroundColor = 'blue'; 
//         }
//         if (event.code === 'KeyZ' && button.textContent === "Z"){
//             for (let btn of buttons) {
//                 btn.style.backgroundColor = 'black';
//             }
//             button.style.backgroundColor = 'blue'; 
//         }
//     }
// })